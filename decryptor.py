import os
import binascii
from cryptography.hazmat.primitives.ciphers.aead import ChaCha20Poly1305, AESGCM

ENC_ALGOS = [
    ('chachapoly', 'ChaCha20Poly1305'),
    ('aesgcm', 'AES-256-GCM')
]


def decryptfile_aes_gcm(filename, key, auth_tag):
    key = binascii.unhexlify(key)
    auth_tag = binascii.unhexlify(auth_tag)
    with open(filename, 'rb') as my_file:
        ciphertext = my_file.read()
        aesgcm = AESGCM(key)
        decrypted = aesgcm.decrypt(auth_tag, ciphertext, None)
        with open(filename.replace(".enc",""), "wb") as my_dec_file:
            my_dec_file.write(decrypted)


def decryptfile_chacha_poly(filename, key, auth_tag):
    key = binascii.unhexlify(key)
    auth_tag = binascii.unhexlify(auth_tag)
    with open(filename, 'rb') as my_file:
        ciphertext = my_file.read()
        chacha = ChaCha20Poly1305(key)
        decrypted = chacha.decrypt(auth_tag, ciphertext, None)
        with open(filename.replace(".enc",""), "wb") as my_dec_file:
            my_dec_file.write(decrypted)



def decryptfile(filename, enc_algo, key, auth_tag):
    if enc_algo == "chachapoly":
        decryptfile_chacha_poly(filename, key, auth_tag)
    elif enc_algo == "aesgcm":
        decryptfile_aes_gcm(filename, key, auth_tag)


if __name__ == '__main__':
    print("Welcome to the our report decryptor!")
    print("####################################")
    print("Encryption algos (input value: homan-readable):")
    print("")
    for algo in ENC_ALGOS:
        print("%s:%s" % algo)

    print("#####################################")
    enc_algo = input("Enc. algo (use input value above): ")
    filename = input("Path to encrypted file: ")
    key = input("Encryption Key: ")
    auth_tag = input("Authentication tag: ")

    decryptfile(filename, enc_algo, key, auth_tag)